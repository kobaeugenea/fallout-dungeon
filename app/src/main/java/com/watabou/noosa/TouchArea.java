/*
 * Copyright (C) 2012-2015 Oleg Dolya
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>
 */

package com.watabou.noosa;

import android.util.Log;

import com.watabou.input.Touchscreen;
import com.watabou.input.Touchscreen.Touch;
import com.watabou.utils.Signal;

public class TouchArea extends Visual implements Signal.Listener<Touchscreen.Touch> {
	
	// Its target can be toucharea itself
	public Visual target;
	
	protected Touchscreen.Touch touch = null;
	
	public TouchArea( Visual target ) {
		super( 0, 0, 0, 0 );
		this.target = target;
		
		Touchscreen.event.add( this );
	}
	
	public TouchArea( float x, float y, float width, float height ) {
		super( x, y, width, height );
		this.target = this;
		
		visible = false;
		
		Touchscreen.event.add( this );
	}

	@Override
	public void onSignal( Touch touch ) {
		
		if (!isActive()) {
			return;
		}

		//Теперь, когда мы уходим с кнопки, она не нажимается при отпускание
//		boolean hit = touch != null && target.overlapsScreenPoint( (int)touch.start.x, (int)touch.start.y );
		boolean hit = touch != null && target.overlapsScreenPoint( (int)touch.current.x, (int)touch.current.y );

		if (hit) {
			//Чтобы все элементы, в область которых попал touch, обрабатывали его
			//Иначе другая area может перехватить touch нажатый не на ней и отменить обработку
			//touch'a в нужной area
//			Touchscreen.event.cancel();

			//нажали на этом элементе
			if (touch.down) {
				if (this.touch == null) {
					this.touch = touch;
				}
				onTouchDown( touch );
			//отпустили на этом элементе
			} else {
				onTouchUp( touch );
				
				if (this.touch == touch) {
					this.touch = null;
					onClick( touch );
				}

			}
			
		} else {
			if (touch == null && this.touch != null
				//Drug снаружи нажатого элемента
				&& !target.overlapsScreenPoint( (int)this.touch.current.x, (int)this.touch.current.y )) {
				onDragOut(this.touch);
			}

			else if (touch == null && this.touch != null
					 //Drug внутри нажатого элемента
					 && target.overlapsScreenPoint( (int)this.touch.current.x, (int)this.touch.current.y )){
				onDragIn(this.touch);
			}
			//отпустили вне нажатого элемента
			else if (this.touch != null && touch != null && !touch.down) {
				onTouchUp( touch );
				this.touch = null;
			}
			
		}
	}
	
	protected void onTouchDown( Touch touch ) {
	}
	
	protected void onTouchUp( Touch touch ) {
	}
	
	protected void onClick( Touch touch ) {
	}
	
	protected void onDragOut(Touch touch) {
	}

	protected void onDragIn(Touch touch) {
	}
	
	public void reset() {
		touch = null;
	}
	
	@Override
	public void destroy() {
		Touchscreen.event.remove( this );
		super.destroy();
	}
}
