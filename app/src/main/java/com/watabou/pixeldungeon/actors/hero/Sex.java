package com.watabou.pixeldungeon.actors.hero;

import com.watabou.pixeldungeon.Assets;

/**
 * Created by Evgeniy on 04.06.2017.
 *
 * Пол
 */
public enum Sex {
    MALE, FEMALE;

    public String spritesheet() {

        switch (this) {
            case MALE:
                return Assets.HUMAN_MALE;
            case FEMALE:
                return Assets.HUMAN_FEMALE;
        }

        return null;
    }
}
