package com.watabou.pixeldungeon.actors;

import com.watabou.pixeldungeon.actors.hero.Sex;

/**
 * Created by Evgeniy on 04.06.2017.
 *
 * Класс человека
 */
public class Human extends Char {

    public Sex sex = Sex.MALE;
}
