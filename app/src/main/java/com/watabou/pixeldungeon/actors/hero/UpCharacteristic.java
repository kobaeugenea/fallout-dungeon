package com.watabou.pixeldungeon.actors.hero;

/**
 * Created by Evgeniy on 08.06.2017.
 *
 * Интерфейс сущностей повышающих характеристики
 */
public interface UpCharacteristic {

    public void up(Hero hero);
}
