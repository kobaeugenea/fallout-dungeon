package com.watabou.pixeldungeon.actors;

/**
 * Created by Evgeniy on 07.06.2017.
 *
 * Skill
 */
public class Skill {

    private int value;
    private boolean isTag;

    public int getValue() {
        return value;
    }

    public void setValue(int value) {
        this.value = value;
    }

    public boolean isTag() {
        return isTag;
    }

    public void setTag(boolean tag) {
        isTag = tag;
    }
}
