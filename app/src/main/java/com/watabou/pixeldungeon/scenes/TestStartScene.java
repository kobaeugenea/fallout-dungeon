/*
 * Pixel Dungeon
 * Copyright (C) 2012-2015 Oleg Dolya
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>
 */
package com.watabou.pixeldungeon.scenes;

import com.watabou.noosa.BitmapTextMultiline;
import com.watabou.noosa.Camera;
import com.watabou.pixeldungeon.PixelDungeon;

//TODO запилить два компонента - выезжающие окно и скролл панель
//удалить options из главного меню, добавить меню загрузки и нвоой игры, options
public class TestStartScene extends PixelScene {
	
	@Override
	public void create() {

		super.create();

		BitmapTextMultiline text = new BitmapTextMultiline(
				"\"Stone\" to his friends, Maxwell is the largest person currently living in Vault " +
				"13. He is known for his physical strength and stamina. He would make the ideal " +
				"volunteer due to his tremendous size and strength. It is unfortunate that his " +
				"intelligence was affected after birth when the labor bot dropped him on his head. " +
				"He doesn't care that he might have to leave the vault.", font1 );
		text.maxWidth = Camera.main.width;
		text.measure();
		add( text );

//		text.x = align( (Camera.main.width - text.width()) / 2 );
//		text.y = align( (Camera.main.height - text.height()) / 2 );

	}


	
	@Override
	protected void onBackPressed() {
		PixelDungeon.switchNoFade( TitleScene.class );
	}
}
