/*
 * Pixel Dungeon
 * Copyright (C) 2012-2015 Oleg Dolya
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>
 */
package com.watabou.pixeldungeon.scenes;

import com.watabou.noosa.BitmapText;
import com.watabou.noosa.Camera;
import com.watabou.noosa.Game;
import com.watabou.noosa.Image;
import com.watabou.noosa.audio.Music;
import com.watabou.noosa.audio.Sample;
import com.watabou.noosa.ui.Button;
import com.watabou.noosa.ui.Component;
import com.watabou.pixeldungeon.Assets;
import com.watabou.pixeldungeon.PixelDungeon;
import com.watabou.pixeldungeon.windows.WndSettings;
import com.watabou.utils.Point;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class TitleScene extends PixelScene {

    private static final String TXT_PLAY = "PLAY";
    private static final String TXT_HIGHSCORES = "RANKINGS";
    private static final String TXT_BADGES = "BADGES";
    private static final String TXT_ABOUT = "ABOUT";
    private static final String TXT_OPTIONS = "OPTIONS";
    private static final String TXT_EXIT = "EXIT";

    @Override
    public void create() {

        super.create();

        Music.INSTANCE.play(Assets.DESERT, true);
        Music.INSTANCE.volume(1f);

        uiCamera.visible = false;

        int w = Camera.main.width;
        int h = Camera.main.height;

        Image background = new Image(Assets.MAINMENU);

        if (PixelDungeon.landscape()) {
            if (background.height / background.width > (float) h / w) {
                background.scale.scale(w / background.width);
            } else {
                background.scale.scale(h / background.height);
            }
            background.y = h / 2 - background.height() / 2;
        } else {
            background.scale.scale(h / background.height);
            background.x = -50 - (w - PixelScene.MIN_WIDTH_P) / 2;
        }
        Buttons buttons = new Buttons();
        buttons.setPos(w / 2 - buttons.width() / 2, h / 2 - buttons.height() / 2);
        add(background);
        add(buttons);

        BitmapText version = new BitmapText("v " + Game.version, font0);
        version.measure();
        version.hardlight(0x9CAC9C);
        version.scale.set(0.57f);
        version.x = w - version.width() - 1;
        version.y = h - version.height();
        add(version);

        fadeIn();
    }

    private class Buttons extends Component {

        private Image image;
        private List<Button> buttons;
        private final List<Point> BUTTON_POSITIONS = Arrays.asList(new Point(9, 4),
                                                                   new Point(9, 29),
                                                                   new Point(9, 55),
                                                                   new Point(9, 81),
                                                                   new Point(9, 108),
                                                                   new Point(9, 134));

        public Buttons() {
            super();

            setSize(image.width(), image.height());
        }

        @Override
        protected void createChildren() {
            super.createChildren();
            image = new Image(Assets.MAINMENU_BG);
            add(image);

            buttons = new ArrayList<>();

            MainMenuButton button = new MainMenuButton(TXT_PLAY) {
                @Override
                protected void onClick() {
                    super.onClick();
                    TitleScene.this.fadeOutWithAction(() -> PixelDungeon.switchScene(StartScene.class));
                }
            };
            buttons.add(button);
            add(button);

            button = new MainMenuButton(TXT_HIGHSCORES) {
                @Override
                protected void onClick() {
                    super.onClick();
                    TitleScene.this.fadeOutWithAction(() -> PixelDungeon.switchScene(RankingsScene.class));
                }
            };
            buttons.add(button);
            add(button);

            button = new MainMenuButton(TXT_BADGES) {
                @Override
                protected void onClick() {
                    super.onClick();
                    TitleScene.this.fadeOutWithAction(() -> PixelDungeon.switchScene(BadgesScene.class));
                }
            };
            buttons.add(button);
            add(button);

            button = new MainMenuButton(TXT_OPTIONS) {
                @Override
                protected void onClick() {
                    super.onClick();
                    parent.add(new WndSettings(false));
//                    TitleScene.this.fadeOutWithAction(() -> parent.add(new WndSettings(false))); //так как создаем его в другой поток, то оно отображается не корректно
                }
            };
            buttons.add(button);
            add(button);

            button = new MainMenuButton(TXT_ABOUT) {
                @Override
                protected void onClick() {
                    super.onClick();
                    TitleScene.this.fadeOutWithAction(() -> PixelDungeon.switchScene(AboutScene.class));
                }
            };
            buttons.add(button);
            add(button);

            button = new MainMenuButton(TXT_EXIT) {
                @Override
                protected void onClick() {
                    super.onClick();
                    TitleScene.this.fadeOutWithAction(() -> Game.instance.finish());
                }
            };
            buttons.add(button);
            add(button);
        }

        @Override
        protected void layout() {
            super.layout();

            image.x = align(x);
            image.y = align(y);

            for (int i = 0; i < BUTTON_POSITIONS.size(); i++) {
                buttons.get(i).setPos(x + BUTTON_POSITIONS.get(i).x, y + BUTTON_POSITIONS.get(i).y);
            }
        }
    }

    private static class MainMenuButton extends Button {

        public static final int WIDTH = 111;
        public static final int HEIGHT = 23;

        private Image up;
        private Image down;
        private BitmapText label;

        public MainMenuButton(String text) {
            super();

            setSize(WIDTH, HEIGHT);

            label.text(text);
            label.measure();
        }

        @Override
        protected void createChildren() {
            super.createChildren();
            up = new Image(Assets.MENUUP);
            down = new Image(Assets.MENUDOWN);
            down.visible = false;

            label = new BitmapText(font4);
            label.hardlight(0xB89C28);
            label.scale.set(0.57f);

            add(up);
            add(down);
            add(label);
        }

        @Override
        protected void layout() {
            super.layout();

            up.x = align(x + 7);
            up.y = align(y + 4);

            down.x = align(x + 7);
            down.y = align(y + 4);

            label.x = align(x + (WIDTH - label.width()) / 2);
            label.y = align(y + 4);
        }

        @Override
        protected void onClick() {
            Sample.INSTANCE.play(Assets.BUTOUT2, 1, 1, 1f);
        }

        @Override
        protected void onTouchDown() {
            Sample.INSTANCE.play(Assets.BUTIN2, 1, 1, 1f);
            down.visible = true;
        }

        @Override
        protected void onTouchUp() {
            down.visible = false;
        }

        @Override
        protected void onDragOut() {
            down.visible = false;
        }

        @Override
        protected void onDragIn() {
            down.visible = true;
        }
    }

    @Override
    protected void onBackPressed() {
        fadeOutWithAction(() -> super.onBackPressed());
    }
}