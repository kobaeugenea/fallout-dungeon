/*
 * Pixel Dungeon
 * Copyright (C) 2012-2015 Oleg Dolya
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>
 */
package com.watabou.pixeldungeon.scenes;

import javax.microedition.khronos.opengles.GL10;

import android.opengl.GLES20;
import android.os.Handler;
import android.util.Log;

import com.watabou.input.Touchscreen;
import com.watabou.noosa.BitmapText;
import com.watabou.noosa.BitmapText.Font;
import com.watabou.noosa.BitmapTextMultiline;
import com.watabou.noosa.Camera;
import com.watabou.noosa.ColorBlock;
import com.watabou.noosa.Game;
import com.watabou.noosa.Scene;
import com.watabou.noosa.Visual;
import com.watabou.pixeldungeon.Assets;
import com.watabou.pixeldungeon.Badges;
import com.watabou.pixeldungeon.PixelDungeon;
import com.watabou.pixeldungeon.effects.BadgeBanner;
import com.watabou.utils.BitmapCache;

public class PixelScene extends Scene {
	
	// Minimum virtual display size for portrait orientation
	public static final float MIN_WIDTH_P		= 128;
	public static final float MIN_HEIGHT_P		= 224;
	
	// Minimum virtual display size for landscape orientation
	public static final float MIN_WIDTH_L		= 224;
	public static final float MIN_HEIGHT_L		= 160;
	
	public static float defaultZoom = 0;
	public static float minZoom;
	public static float maxZoom;
	
	public static Camera uiCamera;
	
	public static BitmapText.Font font0;
	public static BitmapText.Font font1;
	public static BitmapText.Font font2;
	public static BitmapText.Font font3;
	public static BitmapText.Font font4;

	public static float FADE_TIME = 1f;
	
	@Override
	public void create() {
		
		super.create();
		
		GameScene.scene = null;
		
		float minWidth, minHeight;
		if (PixelDungeon.landscape()) {
			minWidth = MIN_WIDTH_L;
			minHeight = MIN_HEIGHT_L;
		} else {
			minWidth = MIN_WIDTH_P;
			minHeight = MIN_HEIGHT_P;
		}

		if (minHeight / minWidth > (float) Game.height / Game.width) {
			defaultZoom = Game.height / minHeight;
		} else {
			defaultZoom = Game.width / minWidth;
		}

		minZoom = 1;
		maxZoom = defaultZoom * 2;	
		
		Camera.reset( new PixelCamera( defaultZoom ) );
		
		float uiZoom = defaultZoom;
		uiCamera = Camera.createFullscreen( uiZoom );
		Camera.add( uiCamera );
		
		if (font0 == null) {
			
			// 3x5 (6)
			font0 = Font.colorMarked(
				BitmapCache.get( Assets.FONT0 ), 9, 0x00000000, BitmapText.Font.LATIN_FULL);
			font0.baseLine = 7;
			font0.tracking = 1;
			
			// 5x8 (10)
			font1 = Font.colorMarked(
					BitmapCache.get( Assets.FONT1 ), 9, 0x00000000, BitmapText.Font.LATIN_FULL);
			font1.baseLine = 7;
			font1.tracking = 1;
			
			// 6x10 (12)
			font2 = Font.colorMarked(
				BitmapCache.get( Assets.FONT2 ), 24, 0x00000000, BitmapText.Font.LATIN_FULL);
			font2.baseLine = 20;
			font2.tracking = 1;
			
			// 7x12 (15)
			font3 = Font.colorMarked(
				BitmapCache.get( Assets.FONT3 ), 16, 0x00000000, BitmapText.Font.LATIN_FULL);
			font3.baseLine = 15;
			font3.tracking = 1;
			
			// 9x15 (18)
			font4 = Font.colorMarked(
				BitmapCache.get( Assets.FONT4 ), 22, 0x00000000, BitmapText.Font.LATIN_FULL);
			font4.baseLine = 21;
			font4.tracking = 1;
		}
	}
	
	@Override
	public void destroy() {
		super.destroy();
		Touchscreen.event.removeAll();
	}
	
	public static BitmapText.Font font;
	public static float scale;
	
	public static void chooseFont( float size ) {
		chooseFont( size, defaultZoom );
	}

	public static void chooseFont( float size, float zoom ) {

		float pt = size * zoom;

		if (pt >= 19) {

			scale = pt / 19;
			if (1.5 <= scale && scale < 2) {
				font = font3;
				scale = (int)(pt / 14);
			} else {
				font = font4;
				scale = (int)scale;
			}

		} else if (pt >= 14) {

			scale = pt / 14;
			if (1.8 <= scale && scale < 2) {
				font = font2;
				scale = (int)(pt / 12);
			} else {
				font = font3;
				scale = (int)scale;
			}

		} else if (pt >= 12) {

			scale = pt / 12;
			if (1.7 <= scale && scale < 2) {
				font = font1;
				scale = (int)(pt / 10);
			} else {
				font = font2;
				scale = (int)scale;
			}

		} else if (pt >= 10) {

			scale = pt / 10;
			if (1.4 <= scale && scale < 2) {
				font = font0;
				scale = (int)(pt / 7);
			} else {
				font = font1;
				scale = (int)scale;
			}

		} else {

			font = font0;
			scale = Math.max( 1, (int)(pt / 7) );

		}

		scale /= zoom;
	}
	
	public static BitmapText createText( float size ) {
		return createText( null, size );
	}
	
	public static BitmapText createText( String text, float size ) {
		
		chooseFont( size );
		
		BitmapText result = new BitmapText( text, font );
		result.scale.set( scale );
		
		return result;
	}
	
	public static BitmapTextMultiline createMultiline( float size ) {
		return createMultiline( null, size );
	}
	
	public static BitmapTextMultiline createMultiline( String text, float size ) {
		
		chooseFont( size );
		
		BitmapTextMultiline result = new BitmapTextMultiline( text, font );
		result.scale.set( scale );
		
		return result;
	}
	
	public static float align( Camera camera, float pos ) {
		return ((int)(pos * camera.zoom)) / camera.zoom;
	}
	
	// This one should be used for UI elements
	public static float align( float pos ) {
		return ((int)(pos * defaultZoom)) / defaultZoom;
	}
	
	public static void align( Visual v ) {
		Camera c = v.camera();
		v.x = align( c, v.x );
		v.y = align( c, v.y );
	}
	
	public static boolean noFade = false;
	protected void fadeIn() {
		if (noFade) {
			noFade = false;
		} else {
			fadeIn( 0xFF000000, false );
		}
	}
	
	protected void fadeIn( int color, boolean light ) {
		add( new Fader( color, light ) );
	}

	protected void fadeOut(){
		fadeOut(0xFF000000, FADE_TIME);
	}

	protected void fadeOut(int color, float time) {
		add( new FaderOut( color, time ) );
	}

	/**
	 * Запускаем затухание и задачу по его окначанию
	 * @param r задача
	 */
	protected void fadeOutWithAction(Runnable r){
		fadeOut();
		new Thread(() -> {
			try {
				Thread.sleep((long) (PixelScene.FADE_TIME * 1000));
			} catch (InterruptedException e) {
				Thread.currentThread().interrupt();
			}
			r.run();
		}).start();
	}
	
	public static void showBadge( Badges.Badge badge ) {
		BadgeBanner banner = BadgeBanner.show( badge.image );
		banner.camera = uiCamera;
		banner.x = align( banner.camera, (banner.camera.width - banner.width) / 2 );
		banner.y = align( banner.camera, (banner.camera.height - banner.height) / 3 );
		Game.scene().add( banner );
	}
	
	protected static class Fader extends ColorBlock {
		
		private boolean light;
		
		private float time;
		
		public Fader( int color, boolean light ) {
			super( uiCamera.width, uiCamera.height, color );
			
			this.light = light;
			
			camera = uiCamera;
			
			alpha( 1f );
			time = FADE_TIME;
		}
		
		@Override
		public void update() {
			
			super.update();
			
			if ((time -= Game.elapsed) <= 0) {
				alpha( 0f );
				parent.remove( this );
			} else {
				alpha( time / FADE_TIME );
			}
		}
		
		@Override
		public void draw() {
			if (light) {
				GLES20.glBlendFunc( GL10.GL_SRC_ALPHA, GL10.GL_ONE );
				super.draw();
				GLES20.glBlendFunc( GL10.GL_SRC_ALPHA, GL10.GL_ONE_MINUS_SRC_ALPHA );
			} else {
				super.draw();
			}
		}
	}

	protected static class FaderOut extends ColorBlock {

		private float time;
		private float fadeTime;

		public FaderOut( int color, float time ) {
			super( uiCamera.width, uiCamera.height, color );

			camera = uiCamera;

			alpha( 0f );

			this.fadeTime = time;
			this.time = time;
		}

		@Override
		public void update() {

			super.update();

			if ((time -= Game.elapsed) <= 0) {
				alpha( 1f );
			} else {
				alpha(1 - time / fadeTime);
			}
		}
	}
	
	private static class PixelCamera extends Camera {
		
		public PixelCamera( float zoom ) {
			super( 
				(int)(Game.width - Math.ceil( Game.width / zoom ) * zoom) / 2, 
				(int)(Game.height - Math.ceil( Game.height / zoom ) * zoom) / 2, 
				(int)Math.ceil( Game.width / zoom ), 
				(int)Math.ceil( Game.height / zoom ), zoom );
		}
		
		@Override
		protected void updateMatrix() {
			float sx = align( this, scroll.x + shakeX );
			float sy = align( this, scroll.y + shakeY );
			
			matrix[0] = +zoom * invW2;
			matrix[5] = -zoom * invH2;
			
			matrix[12] = -1 + x * invW2 - sx * matrix[0];
			matrix[13] = +1 - y * invH2 - sy * matrix[5];
			
		}
	}
}
