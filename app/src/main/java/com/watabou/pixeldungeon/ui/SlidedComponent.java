package com.watabou.pixeldungeon.ui;

import com.watabou.noosa.BitmapText;
import com.watabou.noosa.Game;
import com.watabou.noosa.Image;
import com.watabou.noosa.ui.Component;
import com.watabou.pixeldungeon.Assets;

/**
 * Created by Evgeniy on 20.08.2017.
 *
 * Выпадающий элемент
 */
//TODO подглядывай в scrollComponent, Archs
public class SlidedComponent extends Component {

    //на сколько компонент сдвигается по X
    private int shiftX;
    //на сколько компонент сдвигается по Y
    private int shiftY;
    //за сколько милисекунд элемент "выскользнет"
    private float time = 1000;
    //для состояния SLIDING - время которое еще осталось, чтобы выдвинуться/задвинуться
    private float slidedTime = 0;
    //выскользнул ли уже компонент
    private boolean slide = false;
    //выскализывает ли сейчас компонент
    private boolean sliding = false;

    private Component component;

    /**
     * В какую сторону движется компонент
     */
    private enum Direction{
        VERTICAL, HORIZONTAL
    }

    public SlidedComponent(Component component, Direction direction, int shift) {
        this.component = component;
        switch (direction){
            case VERTICAL: shiftY += shift; break;
            case HORIZONTAL: shiftX += shift; break;
        }
    }

    @Override
    protected void createChildren() {
        super.createChildren();
        add(component);
    }

    @Override
    protected void layout() {
        super.layout();
        component.setPos(x, y);
    }

    @Override
    public void update() {
        super.update();
        if(sliding){
            slidedTime += Game.elapsed * 1000;
            if(slidedTime >= time){
                sliding = false;
                slidedTime = 0;
                slide = !slide;
                if(slide){
                    component.setPos(x + shiftX, y + shiftY);
                } else {
                    component.setPos(x, y);
                }
                return;
            }
            float shiftX = (slidedTime / time) * this.shiftX;
            float shiftY = (slidedTime / time) * this.shiftY;
            if(slide){
                component.setPos(x + shiftX, y + shiftY);
            } else {
                component.setPos(x + (this.shiftX - shiftX), y + (this.shiftY - shiftY));
            }
        }
    }



    public void slide(){
        sliding = true;
    }
}
